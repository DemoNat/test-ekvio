describe('File Upload Component', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Uploads a file and displays its name', () => {
    cy.fixture('example.txt').then(() => {
      cy.get('.file-upload__label').click().selectFile({
        contents: 'cypress/fixtures/example.txt',
        fileName: 'example.txt',
        mimeType: 'text/plain',
      });
    }).then(() => {
      cy.get('.file-upload__text_loaded').should('contain', 'example.txt');
    });
  });
});


