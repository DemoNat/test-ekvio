describe('File Upload Component', () => {
  beforeEach(() => {
    cy.visit('/');
  });

  it('Deletes uploaded file when delete button is clicked', () => {
    const fileName = 'example.txt';
    cy.fixture(fileName).then(() => {
      cy.get('.file-upload__label').click().selectFile({
        contents: 'cypress/fixtures/example.txt',
        fileName: fileName,
        mimeType: 'text/plain',
      });
    })
    cy.get('.file-upload__text_loaded').should('contain', fileName);

    cy.get('.custom-btn').should('contain', 'Удалить').click();

    cy.get('.file-upload__text').should('contain', 'Файл не выбран');
  });
});
